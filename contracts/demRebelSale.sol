pragma solidity >=0.8.0 <0.9.0;

import "@openzeppelin/contracts/access/Ownable.sol";

contract DemRabelSale is Ownable {
    event Transfer(address _from, address _to, uint16 _tokenId);
    event Approval(address _from, address _approved, uint16 _tokenId);
    event ApprovalForAll(address _owner, address _operator, bool _approved);
    
    mapping (address => uint16) countOfDemRebels;
    mapping (uint16  => address) ownerOfDebRebel; 
    
    mapping (uint16  => address) approvedFor;
    mapping (address  => address[]) approvedForAll; 
    
    uint demRabelPrice = 15000000000000000 wei;
    
    uint16 demRableCount = 10000;
    uint16 currentDR     = 0;
    
    uint16 maxBuyCountPerOneOp = 5;
    uint16 maxDemRebelCount    = 10;
    
    function _canTransfer(uint16 _tokenId) private view returns(bool) {
        return (msg.sender == ownerOfDebRebel[_tokenId] 
             || msg.sender == approvedFor[_tokenId]
             || _isApprovedForAll(ownerOfDebRebel[_tokenId], msg.sender));
    }
    
    modifier onlyOwnerOfDemRebel(uint16 _tokenId) {
        require(msg.sender == ownerOfDebRebel[_tokenId], "You are not owner of the DemRebel");
        _;
    }
    
    function balanceOf(address _owner) external view returns (uint16) {
        return countOfDemRebels[_owner];
    }
    
    function ownerOf(uint16 _tokenId) external view returns (address) {
        return ownerOfDebRebel[_tokenId];
    }
    
    function safeTransferFrom(address _from, address _to, uint16 _tokenId) external {
        require(_from != address(0) && _to != address(0) && _canTransfer(_tokenId));
        _transfer(_from, _to, _tokenId);
    }
    
    function transferFrom(address _from, address _to, uint16 _tokenId)  external {
        require(_canTransfer(_tokenId), "Sender can't Transfer this token");
        _transfer(_from, _to, _tokenId);
    }
    
    function _transfer(address _from, address _to, uint16 _tokenId) private {
        require(_canTransfer(_tokenId), "Sender can't Transfer this token");
        ownerOfDebRebel[_tokenId] = _to;
        countOfDemRebels[_from]--;
        countOfDemRebels[_to]++;
        approvedFor[_tokenId] = address(0);
        emit Transfer(_from, _to, _tokenId);
    }
    
    function approve(address _approved, uint16 _tokenId) external onlyOwnerOfDemRebel(_tokenId) {
        approvedFor[_tokenId] = _approved;
        emit Approval(msg.sender, _approved, _tokenId);
    } 
    
    function _isApprovedForAll(address _owner, address _operator) private view returns(bool) {
        for (uint16 i = 0; i < approvedForAll[_owner].length; i++) {
            if (approvedForAll[_owner][i] == _operator) {
                return true;
            }
        }
        return false;
    }
    
    function setApprovalForAll(address _operator, bool _approved) external {
        if (_approved) {
            require(!_isApprovedForAll(msg.sender, _operator), "It's already approved");
            approvedForAll[msg.sender].push(_operator);
        } else {
            require(_isApprovedForAll(msg.sender, _operator), "It's already unapproved");
            for (uint16 i = 0; i < approvedForAll[msg.sender].length; i++) {
                if (approvedForAll[msg.sender][i] == _operator) {
                    approvedForAll[msg.sender][i] = approvedForAll[msg.sender][approvedForAll[msg.sender].length - 1];
                    delete approvedForAll[msg.sender][approvedForAll[msg.sender].length - 1];
                    approvedForAll[msg.sender].pop();
                    break;
                }
            }
        }
        emit ApprovalForAll(msg.sender, _operator, _approved);
    }
    
    function getApproved(uint16 _tokenId) external view returns (address) {
        return approvedFor[_tokenId];
    }
    
    function isApprovedForAll(address _owner, address _operator) external view returns(bool) {
        return _isApprovedForAll(_owner, _operator); 
    }
    
    function _mint(address _to, uint16 _tokenId) private {
        countOfDemRebels[_to]++;
        ownerOfDebRebel[_tokenId] = _to;
        emit Transfer(address(0), _to, _tokenId);
    }
    
    function mint(uint _count) external payable {
        require(msg.value == _count * demRabelPrice, "Wrong value");
        require(_count <= maxBuyCountPerOneOp, "Max count per one operation exceeded");
        require(_count + countOfDemRebels[msg.sender] <= maxDemRebelCount, "Max DemRebel count per one account exceeded");
        require(currentDR + _count <= maxDemRebelCount, "There is no such number of DemRebels");
        for (uint16 i = 0; i < _count; i++) {
            currentDR++;
            _mint(msg.sender, currentDR);
        }
    }
    
    function allTokensOfOwner(address _owner) external view returns(uint16[] memory) {
        uint16[] memory resp = new uint16[](countOfDemRebels[_owner]);
        uint16 j = 0;
        for (uint16 i = 1; i <= currentDR && j < countOfDemRebels[_owner]; i++) {
            if (ownerOfDebRebel[i] == _owner) {
                resp[j] = i;
                j++;
            }
        }
        return resp;
    }
    
    function withdraw() external onlyOwner {
        payable(msg.sender).transfer(address(this).balance);
    }
    
    function getBalance() external onlyOwner view returns(uint) {
        return address(this).balance;
    }
}



